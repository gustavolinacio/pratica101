
import java.io.PrintStream;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author tatog
 */
public class Pratica {
    public static void main(String[] args) {
        long espacolivre;
        long espacototal;
        long memoriatotal;
        Runtime runTime = Runtime.getRuntime();
        System.out.println(System.getProperty("os.name"));
        System.out.println("Nucleos do Processador: " + runTime.availableProcessors());
        espacolivre = runTime.freeMemory()/1048576;
        espacototal = runTime.totalMemory()/1048576;
        memoriatotal = runTime.maxMemory()/1048576;
        System.out.println("Memoria livre: " + espacolivre + " Megabytes");
        System.out.println("Memoria total: " + espacototal + " Megabytes");
        System.out.println("Máxima quantidade de memória usada pela Máquina Virtual: " + memoriatotal + " Megabytes");
    }
}